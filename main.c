#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
//#include <OpenCL/opencl.h>
#include <unistd.h>
#include <CL/cl.h>
#include <sys/resource.h>
#include <sys/time.h>

extern void inicializaGPU(cl_context *context ,cl_command_queue * command_queue, cl_program *program,cl_kernel *kernel, int gpucpu,cl_device_id *device_id,char * programCL);

void errNDRANge(int err){
	if (err==CL_INVALID_PROGRAM_EXECUTABLE){
		printf("CL_INVALID_PROGRAM_EXECUTABLE if there is no successfully built program executable available for device associated with command_queue.. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_COMMAND_QUEUE){
		printf("CL_INVALID_COMMAND_QUEUE if command_queue is not a valid command-queue. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_KERNEL){
		printf("CL_INVALID_KERNEL if kernel is not a valid kernel object. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_CONTEXT){
		printf("CL_INVALID_CONTEXT if context associated with command_queue and kernel is not the same or if the context associated with command_queue and events in event_wait_list are not the same. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_KERNEL_ARGS){
		printf("CL_INVALID_KERNEL_ARGS if the kernel argument values have not been specified. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_WORK_DIMENSION){
		printf("CL_INVALID_WORK_DIMENSION if work_dim is not a valid value (i.e. a value between 1 and 3). Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_WORK_GROUP_SIZE){
		printf("CL_INVALID_WORK_GROUP_SIZE if local_work_size is specified and number of work-items specified by global_work_size is not evenly divisable by size of work-group given by local_work_size or does not match the work-group size specified for kernel using the __attribute__((reqd_work_group_size(X, Y, Z))) qualifier in program source. Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_WORK_GROUP_SIZE){
		printf("CL_INVALID_WORK_GROUP_SIZE if local_work_size is NULL and the __attribute__((reqd_work_group_size(X, Y, Z))) qualifier is used to declare the work-group size for kernel in the program source. Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_WORK_ITEM_SIZE){
		printf("CL_INVALID_WORK_ITEM_SIZE if the number of work-items specified in any of local_work_size[0], ... local_work_size[work_dim - 1] is greater than the corresponding values specified by CL_DEVICE_MAX_WORK_ITEM_SIZES[0], .... CL_DEVICE_MAX_WORK_ITEM_SIZES[work_dim - 1]. Error Code=%d\n", err);
		exit(1);
	}
	if (err==CL_INVALID_GLOBAL_OFFSET){
		printf("CL_INVALID_GLOBAL_OFFSET if global_work_offset is not NULL. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_OUT_OF_RESOURCES){
		printf("CL_OUT_OF_RESOURCES  Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_MEM_OBJECT_ALLOCATION_FAILURE){
		printf("CL_MEM_OBJECT_ALLOCATION_FAILURE if there is a failure to allocate memory for data store associated with image or buffer objects specified as arguments to kernel. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_EVENT_WAIT_LIST){
		printf("CL_INVALID_EVENT_WAIT_LIST if event_wait_list is NULL and num_events_in_wait_list > 0, or event_wait_list is not NULL and num_events_in_wait_list is 0, or if event objects in event_wait_list are not valid events. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_OUT_OF_HOST_MEMORY){
		printf("CL_INVALID_GLOBAL_OFFSET if there is a failure to allocate resources required by the OpenCL implementation on the host. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_MEM_OBJECT){
		printf("CL_INVALID_MEM_OBJECT if buffer is not a valid buffer object. Error Code=%d\n", err);
		exit(1);
	}

	if (err==CL_INVALID_VALUE){
		printf("CL_INVALID_VALUE if the region being written specified by (offset, cb) is out of bounds or if ptr is a NULL value. Error Code=%d\n", err);
		exit(1);
	}
}


double get_time(){
	static struct timeval 	tv0;
	double time_, time;

	gettimeofday(&tv0,(struct timezone*)0);
	time_=(double)((tv0.tv_usec + (tv0.tv_sec)*1000000));
	time = time_/1000000;
	return(time);
}

unsigned char *readBMP(char *file_name, char header[54], int *w, int *h)
{
	//Se abre el fichero en modo binario para lectura
	FILE *f=fopen(file_name, "rb");
	if (!f){
		perror(file_name); exit(1);
	}

	// Cabecera archivo imagen
	//***********************************
	//Devuelve cantidad de bytes leidos
	int n=fread(header, 1, 54, f);

	//Si no lee 54 bytes es que la imagen de entrada es demasiado pequenya
	if (n!=54)
		fprintf(stderr, "Entrada muy pequenia (%d bytes)\n", n), exit(1);

	//Si los dos primeros bytes no corresponden con los caracteres BM no es un fichero BMP
	if (header[0]!='B'|| header[1]!='M')
		fprintf(stderr, "No BMP\n"), exit(1);

	//El tamanyo de la imagen es el valor de la posicion 2 de la cabecera menos 54 bytes que ocupa esa cabecera
	int imagesize=*(int*)(header+2)-54;
	printf("Tamanio archivo = %d\n", imagesize);

	//Si la imagen tiene tamanyo negativo o es superior a 48MB la imagen se rechaza
	if (imagesize<=0|| imagesize > 0x3000000)
		fprintf(stderr, "Imagen muy grande: %d bytes\n", imagesize), exit(1);

	//Si la cabecera no tiene el tamanyo de 54 o el numero de bits por pixel es distinto de 24 la imagen se rechaza
	if (*(int*)(header+10)!=54|| *(short*)(header+28)!=24)
		fprintf(stderr, "No color 24-bit\n"), exit(1);
	
	//Cuando la posicion 30 del header no es 0, es que existe compresion por lo que la imagen no es valida
	if (*(int*)(header+30)!=0)
		fprintf(stderr, "Compresion no suportada\n"), exit(1);
	
	//Se recupera la altura y anchura de la cabecera
	int width=*(int*)(header+18);
	int height=*(int*)(header+22);
	//**************************************


	// Lectura de la imagen
	//*************************************
	unsigned char *image = (unsigned char*)malloc(imagesize+256+width*6); //Se reservan "imagesize+256+width*6" bytes y se devuelve un puntero a estos datos

	unsigned char *tmp;
	image+=128+width*3;
	if ((n=fread(image, 1, imagesize+1, f))!=imagesize)
		fprintf(stderr, "File size incorrect: %d bytes read insted of %d\n", n, imagesize), exit(1);

	fclose(f);
	printf("Image read correctly (width=%i height=%i, imagesize=%i).\n", width, height, imagesize);

	/* Output variables */
	*w = width;
	*h = height;

	return(image);
}

void writeBMP(float *imageFLOAT, char *file_name, char header[54], int width, int height)
{

	FILE *f;
	int i, n;

	int imagesize=*(int*)(header+2)-54;

	unsigned char *image = (unsigned char*)malloc(3*sizeof(unsigned char)*width*height);

	for (i=0;i<width*height;i++){
		image[3*i]   = imageFLOAT[i]; //R 
		image[3*i+1] = imageFLOAT[i]; //G
		image[3*i+2] = imageFLOAT[i]; //B
	}
	

	f=fopen(file_name, "wb");		//Se abre el fichero en modo binario de escritura
	if (!f){
		perror(file_name); 
		exit(1);
	}

	n=fwrite(header, 1, 54, f);		//Primeramente se escribe la cabecera de la imagen
	n+=fwrite(image, 1, imagesize, f);	//Y despues se escribe el resto de la imagen
	if (n!=54+imagesize)			//Si se han escrito diferente cantidad de bytes que la suma de la cabecera y el tamanyo de la imagen. Ha habido error
		fprintf(stderr, "Escritos %d de %d bytes\n", n, imagesize+54);
	fclose(f);

	free(image);

}


float *RGB2BW(unsigned char *imageUCHAR, int width, int height)
{
	int i, j;
	float *imageBW = (float *)malloc(sizeof(float)*width*height);

	unsigned char R, B, G;

	for (i=0; i<height; i++)
		for (j=0; j<width; j++)
		{
			R = imageUCHAR[3*(i*width+j)];
			G = imageUCHAR[3*(i*width+j)+1];
			B = imageUCHAR[3*(i*width+j)+2];

			imageBW[i*width+j] = 0.2989 * R + 0.5870 * G + 0.1140 * B;
		}

	return(imageBW);
}

#define MAX_WINDOW_SIZE 5*5

void mergeSort(float arr[],int low,int mid,int high){

    int i,m,k,l;
    float temp[MAX_WINDOW_SIZE];

    l=low;
    i=low;
    m=mid+1;

    while((l<=mid)&&(m<=high)){

         if(arr[l]<=arr[m]){
             temp[i]=arr[l];
             l++;
         }
         else{
             temp[i]=arr[m];
             m++;
         }
         i++;
    }

    if(l>mid){
         for(k=m;k<=high;k++){
             temp[i]=arr[k];
             i++;
         }
    }
    else{
         for(k=l;k<=mid;k++){
             temp[i]=arr[k];
             i++;
         }
    }
   
    for(k=low;k<=high;k++){
         arr[k]=temp[k];
    }
}

void buble_sort(float array[], int size)
{
	int i, j;
	float tmp;

	for (i=1; i<size; i++)
		for (j=0 ; j<size - i; j++)
			if (array[j] > array[j+1]){
				tmp = array[j];
				array[j] = array[j+1];
				array[j+1] = tmp;
			}
}

void remove_noise(float *im, float *image_out, 
	float thredshold, int window_size,
	int height, int width)
{
	int i, j, ii, jj;

	float window[MAX_WINDOW_SIZE];
	float median;
	int ws2 = (window_size-1)>>1; 

	for(i=ws2; i<height-ws2; i++)
		for(j=ws2; j<width-ws2; j++)
		{
			for (ii =-ws2; ii<=ws2; ii++)
				for (jj =-ws2; jj<=ws2; jj++)
					window[(ii+ws2)*window_size + jj+ws2] = im[(i+ii)*width + j+jj];

			// SORT
			buble_sort(window, window_size*window_size);
			median = window[(window_size*window_size-1)>>1+1];

			if (fabsf((median-im[i*width+j])/median) <=thredshold)
				image_out[i*width + j] = im[i*width+j];
			else
				image_out[i*width + j] = window[(window_size*window_size-1)>>1+1];

				
		}
}

void remove_noiseCPUHeterogeneo(float *im, float *image_out,float thredshold, int window_size,int height, int width)
{
	cl_device_id device_idCPU;     // compute device id
	cl_context contextCPU;       // compute context	
	cl_command_queue command_queueCPU;
	cl_program programCPU;       // compute program
	cl_kernel kernelCPU;


	cl_device_id device_idGPU;     // compute device id
	cl_context contextGPU;       // compute context	
	cl_command_queue command_queueGPU;
	cl_program programGPU;       // compute program
	cl_kernel kernelGPU;
	int err,err1;               // error code returned from OpenCL calls
	size_t globalCPU[2]; 
	size_t globalGPU[2];              // global domain size
	size_t local[2];               // local domain size

	cl_mem d_imCPU;
	cl_mem d_imGPU;
	cl_mem d_outGPU;
	cl_mem d_outCPU;

	//printf("Empezando la  parte heterogenea\n");

	inicializaGPU(&contextCPU ,&command_queueCPU,&programCPU,&kernelCPU, 0 ,&device_idCPU,"saltAndPeper.cl");
	inicializaGPU(&contextGPU ,&command_queueGPU,&programGPU,&kernelGPU, 0 ,&device_idGPU,"saltAndPeper.cl");

	//printf("Ya he incializado GPU y CPU\n");
	int offset=1;
	d_imCPU = clCreateBuffer(contextCPU, CL_MEM_READ_ONLY, sizeof(float)*(height+1)*width/4, NULL, NULL);
	d_imGPU = clCreateBuffer(contextGPU, CL_MEM_READ_ONLY, sizeof(float)*(height+offset)*width*3/4, NULL, NULL);
	d_outCPU = clCreateBuffer(contextCPU, CL_MEM_WRITE_ONLY, sizeof(float)*(height+1)*width/4, NULL,NULL);
	d_outGPU = clCreateBuffer(contextGPU, CL_MEM_WRITE_ONLY, sizeof(float)*(height+offset)*width*3/4, NULL,NULL);


	if (!d_imCPU || !d_imGPU || !d_outCPU|| !d_outGPU){
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}


	//en la CPU leemos desde la posicion 0 hasta sizeof(float)*height*width/2
	err = clEnqueueWriteBuffer(command_queueCPU, d_imCPU, CL_TRUE, 0, sizeof(float)*(height+1)*width/4 ,im  , 0, NULL, NULL);
	printf("clEnqueueWriteBuffer CPU \n");
	errNDRANge(err);

	//en la GPU leemos desde la poos sizeof(float)*height*width/2 hasta el fin
	err1 = clEnqueueWriteBuffer(command_queueGPU, d_imGPU, CL_TRUE, 0 , sizeof(float)*(height+offset)*width*3/4,&im[((height-offset)/4)*width] , 0, NULL, NULL);	
	printf("clEnqueueWriteBuffer GPU \n");
	errNDRANge(err1);


	int heightCPU = (height+1) / 4;
	int heightGPU = (height+offset)*3/4;
	globalCPU[0] = heightCPU;
	globalCPU[1] = width;

	globalGPU[0] = heightGPU;
	globalGPU[1] = width;	

	
	if (clSetKernelArg(kernelCPU, 0, sizeof(cl_mem), &d_imCPU) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernelCPU, 1, sizeof(cl_mem), &d_outCPU)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 1\n");
	if (clSetKernelArg(kernelCPU, 2, sizeof(cl_float), &thredshold)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 2\n");
	if (clSetKernelArg(kernelCPU, 3, sizeof(cl_int), &heightCPU)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 3\n");
	if (clSetKernelArg(kernelCPU, 4, sizeof(cl_int), &width)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 4\n");
	if (clSetKernelArg(kernelCPU, 5, sizeof(cl_int), &window_size)!= CL_SUCCESS)		
		printf("a fallado el setKernelArg 5\n");	


	//printf("ya He puesto las variables al kernel CPU\n");	


	if (clSetKernelArg(kernelGPU, 0, sizeof(cl_mem), &d_imGPU) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernelGPU, 1, sizeof(cl_mem), &d_outGPU)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 1\n");
	if (clSetKernelArg(kernelGPU, 2, sizeof(cl_float), &thredshold)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 2\n");
	if (clSetKernelArg(kernelGPU, 3, sizeof(cl_int), &heightGPU)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 3\n");
	if (clSetKernelArg(kernelGPU, 4, sizeof(cl_int), &width)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 4\n");
	if (clSetKernelArg(kernelGPU, 5, sizeof(cl_int), &window_size)!= CL_SUCCESS)		
		printf("a fallado el setKernelArg 5\n");	
	//printf("ya He puesto las variables al kernel GPU\n");
	

	err = clEnqueueNDRangeKernel(command_queueCPU, kernelCPU, 2, NULL, globalCPU, NULL,0, NULL, NULL);
	err1 = clEnqueueNDRangeKernel(command_queueGPU, kernelGPU, 2, NULL, globalGPU, NULL,0, NULL, NULL);
	printf("clEnqueueNDRangeKernel CPU \n");
	errNDRANge(err);
	printf("clEnqueueNDRangeKernel GPU \n");
	errNDRANge(err1);
	

	clFinish(command_queueCPU);
	//clFinish(command_queueGPU);
	
	err = clEnqueueReadBuffer( command_queueCPU, d_outCPU, CL_TRUE, 0, sizeof(float)*(height+1)*width/4 , image_out , 0, NULL, NULL );
	err1 = clEnqueueReadBuffer( command_queueGPU, d_outGPU, CL_TRUE, 0, sizeof(float)*(height+offset)*width*3/4, &image_out[((height-offset)/4)*width]   , 0, NULL, NULL );
	printf("clEnqueueReadBuffer CPU \n");
	errNDRANge(err); 
	printf("clEnqueueReadBuffer GPU \n");
	errNDRANge(err1);

	clReleaseProgram(programCPU);	
	clReleaseKernel(kernelCPU);	
	clReleaseCommandQueue(command_queueCPU);	
	clReleaseContext(contextCPU);
	clReleaseMemObject(d_outCPU);   
	clReleaseMemObject(d_imCPU); 

	clReleaseProgram(programGPU);	
	clReleaseKernel(kernelGPU);	
	clReleaseCommandQueue(command_queueGPU);	
	clReleaseContext(contextGPU);
	clReleaseMemObject(d_outGPU);   
	clReleaseMemObject(d_imGPU);   
	  
	
	 	
}



void remove_noiseCPU(float *im, float *image_out,float thredshold, int window_size,int height, int width, int cpu)
{
	
	cl_device_id device_id;     // compute device id
	cl_context context;       // compute context	
	cl_command_queue command_queue;
	cl_program program;       // compute program
	cl_kernel kernel;
	int err;               // error code returned from OpenCL calls
	size_t global[2];               // global domain size
	size_t local[2];               // local domain size

	cl_mem d_im;
	cl_mem d_out;

	inicializaGPU(&context ,&command_queue,&program,&kernel, cpu ,&device_id,"saltAndPeper.cl");		


	
	d_im = clCreateBuffer(context, CL_MEM_READ_ONLY , sizeof(float)*height*width, NULL, NULL);
	d_out = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float)*height*width, NULL,NULL);

	if (!d_im || !d_out){
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}
	/*float *imDiv3 = im;
	for (int i=0;i<sizeof(float)*height*width/2;i++){
			imDiv3++;
	}*/

	err=clEnqueueWriteBuffer(command_queue, d_im, CL_TRUE, 0, sizeof(float)*height*width,im, 0, NULL, NULL);
	errNDRANge(err);
	clFinish(command_queue);


	if (clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_im) != CL_SUCCESS)
		printf("ha fallado el setKernelArg 0\n");
	if (clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_out)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 1\n");
	if (clSetKernelArg(kernel, 2, sizeof(cl_float), &thredshold)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 2\n");
	if (clSetKernelArg(kernel, 3, sizeof(cl_int), &height)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 3\n");
	if (clSetKernelArg(kernel, 4, sizeof(cl_int), &width)!= CL_SUCCESS)
		printf("a fallado el setKernelArg 4\n");
	if (clSetKernelArg(kernel, 5, sizeof(cl_int), &window_size)!= CL_SUCCESS)		
		printf("a fallado el setKernelArg 5\n");	


	global[0] = height;
	global[1] = width;

	//local[0]=5;
	//local[1]=5;
	
	err = clEnqueueNDRangeKernel(command_queue, kernel, 2, NULL, global, NULL,0, NULL, NULL);
	
	errNDRANge(err);

	clFinish(command_queue);
	

	 if (clEnqueueReadBuffer( command_queue, d_out, CL_TRUE, 0, sizeof(float)*height*width, image_out  , 0, NULL, NULL )!=CL_SUCCESS)
	 	printf("ha fallado la lectura de pixel\n");	 

	clReleaseProgram(program);	
	clReleaseKernel(kernel);	
	clReleaseCommandQueue(command_queue);	
	clReleaseContext(context);
	clReleaseMemObject(d_out);   
	clReleaseMemObject(d_im);   
	

}




void freeMemory(unsigned char *imageUCHAR, float *imageBW, float *imageOUT)
{
	//free(imageUCHAR);
	free(imageBW);
	free(imageOUT);

}	


int main(int argc, char **argv) {

	int width, height;
	unsigned char *imageUCHAR;
	float *imageBW;

	char header[54];


	//Variables para calcular el tiempo
	double t0, t1;
	double cpu_time_used = 0.0;

	//Tener menos de 3 argumentos es incorrecto
	if (argc < 4) {
		fprintf(stderr, "Uso incorrecto de los parametros. exe  input.bmp output.bmp [cg]\n");
		exit(1);
	}


	// READ IMAGE & Convert image
	imageUCHAR = readBMP(argv[1], header, &width, &height);
	imageBW = RGB2BW(imageUCHAR, width, height);


	// Aux. memory
	float *imageOUTGPU = (float *)malloc(sizeof(float)*width*height);
	float *imageOUT = (float *)malloc(sizeof(float)*width*height);
	float *imageOUTHeterogeneo = (float *)malloc(sizeof(float)*width*height);

	////////////////
	// CANNY      //
	////////////////
	switch (argv[3][0]) {
		case 'c':
			t0 = get_time();
			remove_noise(imageBW, imageOUT,
				0.1, 3, height, width);
			t1 = get_time();
			printf("CPU Exection time %f ms.\n", t1-t0);
			writeBMP(imageOUT, argv[2], header, width, height);
			freeMemory(imageUCHAR, imageBW, imageOUT);	
			break;
		case 'g':
			/*t0 = get_time();			
			remove_noiseCPU(imageBW, imageOUT,0.1, 3, height, width,0);
			t1 = get_time();
			char buf[256];
			snprintf(buf, sizeof buf, "%s%s", argv[2], "GPUProcesador.bmp");
			writeBMP(imageOUT, buf, header, width, height);
			printf("GPU procesador execution time %f ms.\n", t1-t0);
			


			//-----------------GPU CO procesador------------------

			t0 = get_time();			
			remove_noiseCPU(imageBW, imageOUTGPU,0.1, 3, height, width,0);
			t1 = get_time();
			char buf1[256];
			snprintf(buf1, sizeof buf1, "%s%s", argv[2], "GPUCOProcesador.bmp");
			writeBMP(imageOUTGPU, buf1, header, width, height);
			printf("GPU COprocesador execution time %f ms.\n", t1-t0);*/


			t0 = get_time();	

			remove_noiseCPUHeterogeneo(imageBW, imageOUTHeterogeneo,0.1, 3, height, width);
			t1 = get_time();
			char buf2[256];
			snprintf(buf2, sizeof buf2, "%s%s", argv[2], "GPUHeterogeneo.bmp");
			writeBMP(imageOUTHeterogeneo, buf2, header, width, height);
			printf("GPUHeterogeneo execution time %f ms.\n", t1-t0);
			
			free(imageOUTGPU);
			free(imageOUT);
			free(imageOUTHeterogeneo);
			free(imageBW);



			
			break;
		default:
			printf("Not Implemented yet!!\n");


	}

	
	
	
}




