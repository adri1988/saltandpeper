#define MAX_WINDOW_SIZE 5*5

void buble_sort(float array[], int size)
{
	int k, l;
	float tmp;

	for (k=1; k<size; k++)
		for (l=0 ; l<size - k; l++)
			if (array[l] > array[l+1]){
				tmp = array[l];
				array[l] = array[l+1];
				array[l+1] = tmp;
			}
}

__kernel
void saltAndPeper(__global float *im,__global float *image_out,const float thredshold,const int height,const int width,const int window_size)
{
	int ii, jj;

	float window[MAX_WINDOW_SIZE];
	float median;
	int ws2 = (window_size-1)>>1; 

	int i = get_global_id(0);
	int j = get_global_id(1);

	if (i>=ws2 && i<height-ws2 && j >= ws2 && j< width-ws2 ){	

		for (ii =-ws2; ii<=ws2; ii++)
			for (jj =-ws2; jj<=ws2; jj++)
				window[(ii+ws2)*window_size + jj+ws2] = im[(i+ii)*width + j+jj];

		// SORT
		buble_sort(window, window_size*window_size);

		median = window[(window_size*window_size-1)>>1+1];

		if (fabs((median-im[i*width+j])/median) <=thredshold)
			image_out[i*width + j] = im[i*width+j];
		else
			image_out[i*width + j] = window[(window_size*window_size-1)>>1+1];

				
		}
			
}

/*__kernel
void saltAndPeper(__global float *im,__global float *image_out,const float thredshold,const int height,const int width,const int window_size)
{
	int ii, jj;

	float window[MAX_WINDOW_SIZE];
	float median;
	int ws2 = (window_size-1)>>1; 

	int i = get_global_id(0);
	int j = get_global_id(1);

	if (i>=ws2 && i<height-ws2 && j >= ws2 && j< width-ws2 ){	

		for (ii =-ws2; ii<=ws2; ii++)
			for (jj =-ws2; jj<=ws2; jj++)
				window[(ii+ws2)*window_size + jj+ws2] = im[(i+ii)*width + j+jj];

		// SORT
		buble_sort(window, window_size*window_size);
		median = window[(window_size*window_size-1)>>1+1];

		if (fabs((median-im[i*width+j])/median) <=thredshold)
			image_out[i*width + j] = im[i*width+j];
		else
			image_out[i*width + j] = window[(window_size*window_size-1)>>1+1];

				
		}
			
}*/